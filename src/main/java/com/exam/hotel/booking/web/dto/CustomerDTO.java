package com.exam.hotel.booking.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@NoArgsConstructor(force=true)
@AllArgsConstructor(staticName="of")
public class CustomerDTO {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  Long _id;

  String name;

  String address;

  String email;

  boolean isTravelAgency;
}
