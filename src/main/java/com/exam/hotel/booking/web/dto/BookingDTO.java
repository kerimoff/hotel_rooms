package com.exam.hotel.booking.web.dto;

import com.exam.hotel.booking.model.BookingStatus;
import com.exam.hotel.common.web.dto.BusinessPeriodDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor(force=true)
@AllArgsConstructor(staticName="of")
public class BookingDTO {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  Long _id;

  BigDecimal totalCost;

  BookingStatus status;

  CustomerDTO customer;

  List<RoomDTO> rooms;

  BusinessPeriodDTO businessPeriodDTO;
}
