INSERT INTO customer (id, name, address, email, is_travel_agency) VALUES (1, 'a', 'address1', 'email1@a.a', false);
INSERT INTO customer (id, name, address, email, is_travel_agency) VALUES (2, 'b', 'address2', 'email2@b.b', false);
INSERT INTO customer (id, name, address, email, is_travel_agency) VALUES (3, 'c', 'address3', 'email1@c.c', true);

INSERT INTO room (id, cost, type) VALUES (1, 100, 'SINGLE');
INSERT INTO room (id, cost, type) VALUES (2, 200, 'DOUBLE');
INSERT INTO room (id, cost, type) VALUES (3, 300, 'TWIN');
INSERT INTO room (id, cost, type) VALUES (4, 400, 'STUDIO');

INSERT INTO booking (id, total_cost, status, customer_id, start_date, end_date) VALUES (1, 1000, 'CONFIRMED', 1,  '2017-03-22', '2017-03-24');

INSERT INTO booking_rooms (booking_id, rooms_id) VALUES (1, 1);
INSERT INTO booking_rooms (booking_id, rooms_id) VALUES (1, 2);
INSERT INTO booking_rooms (booking_id, rooms_id) VALUES (1, 3);
