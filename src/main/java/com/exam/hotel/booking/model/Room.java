package com.exam.hotel.booking.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Entity
@NoArgsConstructor(force=true)
@AllArgsConstructor(staticName="of")
public class Room {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  Long id;

  @Column(scale = 2, precision = 8)
  BigDecimal cost;

  @Enumerated(EnumType.STRING)
  RoomType type;
}
