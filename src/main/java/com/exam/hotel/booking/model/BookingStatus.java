package com.exam.hotel.booking.model;

public enum BookingStatus {
  CONFIRMED, CANCELLED, PENDING;
}
