package com.exam.hotel.booking.model;

public enum RoomType {
  SINGLE, DOUBLE, TWIN, STUDIO;
}
