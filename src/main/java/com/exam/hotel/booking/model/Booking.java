package com.exam.hotel.booking.model;

import com.exam.hotel.common.domain.model.BusinessPeriod;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Data
@Entity
@NoArgsConstructor(force=true)
@AllArgsConstructor(staticName="of")
public class Booking {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  Long id;

  @Column(scale = 2, precision = 8)
  BigDecimal totalCost;

  @Enumerated(EnumType.STRING)
  BookingStatus status;

  @ManyToOne
  Customer customer;

  @OneToMany
  List<Room> rooms;

  @Embedded
  BusinessPeriod businessPeriod;
}
