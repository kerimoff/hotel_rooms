package com.exam.hotel.common.domain.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Value;

import javax.persistence.Embeddable;
import java.time.LocalDate;

@Embeddable
@Value
@NoArgsConstructor(force=true)
@AllArgsConstructor(staticName="of")
public class BusinessPeriod {

  private LocalDate startDate;

  private LocalDate endDate;
}
