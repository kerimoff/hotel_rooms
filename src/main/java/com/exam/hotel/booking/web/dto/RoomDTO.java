package com.exam.hotel.booking.web.dto;

import com.exam.hotel.booking.model.RoomType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;

@Data
@NoArgsConstructor(force=true)
@AllArgsConstructor(staticName="of")
public class RoomDTO {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  Long _id;

  BigDecimal cost;

  RoomType type;
}
